Node.js version: 8.11.1

NPM version: 5.6.0

To run the app:

    npm install

    npm start

Generated output file is ./formatteraddresses.txt

To run unit tests:

    npm install

    npm test

Notes:

    I didn't purchase API key so it has very limited requests allowed per day. So if you want to use another API key you can do the following:
    If you're running the app under linux, please run the app following these steps:

    1. Open a terminal

    2. export GEO_CODE_API_KEY=[your own API key]

    3. npm start
