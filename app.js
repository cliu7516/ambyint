const util = require('util');
const readline = require('readline');
const fs = require('fs');
const processSchedulerClass = require('./lib/processScheduler.js');
const processScheduler = new processSchedulerClass();

function main() {
    let rl = readline.createInterface({
        input: fs.createReadStream(`${__dirname}/addresses.txt`)
    });

    rl.on('line', (line) => {
        processScheduler.startJob(line);
    });

    rl.on('close', () => {
        processScheduler.finish();
    });

}

main();