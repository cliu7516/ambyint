const util = require('util');
const proxy = require('proxyquire');
const sinon = require('sinon');
const should = require('should');
const chai = require('chai');

describe('addressGeoCoder', () => {
    let geoCoder;
    let promiseRequestMock;
    let promiseRequestMockFunction;
    beforeEach(async () => {
        promiseRequestMock = async function() {
            return await promiseRequestMockFunction();
        };
        geoCoder = proxy('../../lib/addressGeoCoder.js', {'request-promise': promiseRequestMock});
    });

    describe('When calling geoCode', () => {
        let geoCodeRes = null;

        beforeEach(async () => {
            geoCodeRes = {
                status: 'OK',
                results: [
                    {
                        geometry: {
                            location_type: 'ROOFTOP'
                        },
                        formatted_address: 'Test Address'
                    }
                ]
            }
        });
        it('should return formatted address if it is successful', async () => {
            promiseRequestMockFunction = sinon.fake.resolves(geoCodeRes);
            let result = await geoCoder.run({City: 'New York'});
            result.should.be.eql('Test Address');
        })

        it('should return null if the quality is not ROOFTOP', async () => {
            geoCodeRes.results[0].geometry.location_type = 'NONEROOFTOP';
            promiseRequestMockFunction = sinon.fake.resolves(geoCodeRes);
            let result = await geoCoder.run({City: 'New York'});
            chai.expect(result).to.be.null;
        })

        it('should retry if http call fails randomly', async () => {
            let httpCallTimes = 0;
            promiseRequestMockFunction = () => {
                return new Promise((resolve, reject) => {
                    if (0 === httpCallTimes++) {
                        reject(new Error('test'));
                    } else {
                        resolve(geoCodeRes);
                    }
                })
            }
            let result = await geoCoder.run({City: 'New York'});
            result.should.be.eql('Test Address');
            httpCallTimes.should.be.eql(2);
        })

    });
});