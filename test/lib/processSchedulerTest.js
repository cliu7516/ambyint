const util = require('util');
const proxy = require('proxyquire');
const sinon = require('sinon');
const should = require('should');
const chai = require('chai');
const assertArrays = require('chai-arrays');
chai.use(assertArrays);
const config = require('../../config/config.js');

describe('processScheduler', () => {
    let scheduler = null;
    let onDoWork;
    let delay = util.promisify(setTimeout);
    class workerMock {
        async doWork(payload) {
            await onDoWork(payload);
        }
    }
    class writerMock {
        async writeAddress(address) {
        }
        async finishWriting() {
        }
    }
    beforeEach(async () => {
        promiseRequestMock = async function() {
            return await promiseRequestMockFunction();
        };
        let schedulerClass = proxy('../../lib/processScheduler.js', {
            './addressProcessWorker.js': workerMock,
            './addressWriter.js': writerMock
        });
        scheduler = new schedulerClass();
    });
    describe('When running jobs', () => {
        let allProcessedJobs = [];
        beforeEach(async () => {
            allProcessedJobs = [];
            onDoWork = async function(payload) {
                allProcessedJobs.push(payload);
            }
        });

        it('should run all jobs', async () => {
            scheduler.startJob('1');
            scheduler.startJob('2');
            scheduler.startJob('3');
            await delay(50);
            chai.expect(allProcessedJobs).to.be.ofSize(3);
            chai.expect(allProcessedJobs).to.be.containing('1');
            chai.expect(allProcessedJobs).to.be.containing('2');
            chai.expect(allProcessedJobs).to.be.containing('3');
        })

        it('should queue jobs and eventually finish all jobs after workers are available', async () => {
            onDoWork = async function(payload) {
                allProcessedJobs.push(payload);
                await delay(500)
            }
            let totalJobs = config.maxNumberOfWorkers + 10;
            for (let i = 0; i < totalJobs; i++) {
                scheduler.startJob('Test');           
            }
            await delay(100);
            chai.expect(allProcessedJobs).to.be.ofSize(config.maxNumberOfWorkers)
            await delay(2000);
            chai.expect(allProcessedJobs).to.be.ofSize(totalJobs)
        })

    });
});