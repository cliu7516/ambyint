const util = require('util');
const config = require('../config/config.js');
const workerClass = require('./addressProcessWorker.js');
const addressWriterClass = require('./addressWriter.js');

class processScheduler {
    constructor() {
        this._workersPool = [];
        this._addressWriter = new addressWriterClass(config.outputFileName);
        for (let i = 0; i < config.maxNumberOfWorkers; i++) {
            this._workersPool.push(new workerClass(this._addressWriter));      
        }
        this._pendingJobs = [];
    }

    async runWorker(worker) {
        if (this._pendingJobs.length > 0) {
            let job = this._pendingJobs.shift();
            await worker.doWork(job);
            await this.runWorker(worker);
        } else {
            this._workersPool.push(worker);
        }
    }

    startJob(payload) {
        this._pendingJobs.push(payload);
        if (this._workersPool.length > 0) {
            let worker = this._workersPool.pop();
            this.runWorker(worker);
        }
}

    async finish() {
        await this._addressWriter.finishWriting();
    }
}

module.exports = processScheduler;