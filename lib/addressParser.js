const http = require('request');

var addressMapper = [
    {
        name: "houseNumber",
        length: 30
    }, {
        name: "streetDirectionPrefix",
        length: 2
    }, {
        name: "streetName",
        length: 40
    }, {
        name: "streetSuffix",
        length: 4
    }, {
        name: "streetDirectionSuffix",
        length: 2
    }, {
        name: "unitDescriptor",
        length: 10
    }, {
        name: "unitNumber",
        length: 6
    }, {
        name: "city",
        length: 30
    }, {
        name: "state",
        length: 2
    }, {
        name: "zipCode",
        length: 12
    }
]

async function parse (addressLine) {
    var address = {};
    let pos = 0;
    for (let i = 0; i < addressMapper.length; i++) {
        const element = addressMapper[i];
        if (addressLine.length >= pos + element.length) {
            address[element.name] = addressLine.substr(pos, element.length).trim();
            pos += element.length;
        } else {
            address = null;
            break;
        }
    }
    return address;
}

exports.parse = parse;