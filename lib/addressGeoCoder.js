const httpRequest = require('request-promise');
const promiseRetry = require('promise-retry');
const config = require('../config/config.js');

async function callGeoCode(address) {
    let addressParam = '';
    for (var addressComponent in address) {
        if (address[addressComponent]) {
            if (addressParam) {
                addressParam += '+' + address[addressComponent]
            } else {
                addressParam += address[addressComponent]
            }
        }
    }
    let options = {
        uri: `https://maps.googleapis.com/maps/api/geocode/json?address=${addressParam}&key=${config.geoCodeApiKey}`,
        method: 'GET',
        json: true,
        timeout: 120 * 1000   // 2 minutes
    }

    return await httpRequest(options);

}

async function geoCode(address) {
    let retryOptions = {
        retries: config.numberOfRetries,
        factor: 2,
        minTimeout: 1000,             // 1 second
        maxTimeout: 1000,
        randomize: true
    };
    let geoCodeResponse = await promiseRetry((retry) => {
        return callGeoCode(address).catch((err)=> {
            retry();            
        });
    }, retryOptions);
    let result = null;
    if (geoCodeResponse.status === 'OK' && geoCodeResponse.results.length > 0) {
        let geoCodeResult = geoCodeResponse.results[0];
        if (geoCodeResult.geometry.location_type === 'ROOFTOP') {
            result = geoCodeResult.formatted_address;
        }
    }

    return result;
}

module.exports.run = geoCode;
