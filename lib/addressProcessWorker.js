const addressParser = require('./addressParser.js');
const addressGeoCoder = require('./addressGeoCoder');

class addressProcessWorker {
    constructor(addressWriter) {
        this._addressWriter = addressWriter;
    }
    async doWork(payload) {
        let address = await addressParser.parse(payload);
        if (address) {
            let geoAddress = await addressGeoCoder.run(address);
            if (geoAddress) {
                await this._addressWriter.writeAddress(geoAddress);
            }
        }
    }
}

module.exports = addressProcessWorker;