const util = require('util');
const fs = require('fs');
const endOfLine = require('os').EOL;

class addressWriter {
    constructor(file) {
        this._writeStream = fs.createWriteStream(file);
    }
    async writeAddress(address) {
        this._writeStream.write(address);
        this._writeStream.write(endOfLine);
    }
    async finishWriting() {
        this._writeStream.close()
    }
}

module.exports = addressWriter
