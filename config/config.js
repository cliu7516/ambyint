module.exports = {
    maxNumberOfWorkers: process.env.MAX_NUMBER_OF_WORKERS || 20,
    geoCodeApiKey: process.env.GEO_CODE_API_KEY || 'AIzaSyAdOrqCW8PF4SQV7aIYyR4RNhoTGqNgsos',
    numberOfRetries: process.env.NUM_RETRY || 3,
    outputFileName: process.env.OUTPUT_FILE || 'formattedAddresses.txt'
}